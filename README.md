# Welcome to your Expo app 👋

This is an [Expo](https://expo.dev) project created with [`create-expo-app`](https://www.npmjs.com/package/create-expo-app).

## Get started

requisites:
 node version 18.19 or newer (https://nodejs.org/en/download/package-manager)

1. Install dependencies

   ```bash
   npm install
   ```

2. Start the app

   ```bash
    npx expo start
   ```
3. Install Expo Go in device 

   available in play store and app store 

4. Read Qr from npx expo start

   After we run the command npx expo start we will able to see a Qr code in our terminal.
   Open camera and read the Qr, we will be redirected to the expo app and asked to allow permission 

5. Open application and see the result for the list item and the map

   in the first Tab we will see the items list and we can switch tab to see the map. For the map we will asked for location permission.


I attached a video of the functionaity of the app:

      https://drive.google.com/drive/folders/1HK3nErZHZDXCpAqLuIVlqMbjAV6bilQY?usp=sharing

      