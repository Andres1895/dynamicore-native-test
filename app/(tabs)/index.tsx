import React, { useState } from "react";
import {
  View,
  Text,
  TextInput,
  Button,
  FlatList,
  StyleSheet,
} from "react-native";

type ItemType = {
  id: string;
  value: string;
};
const App = () => {
  const [items, setItems] = useState<ItemType[]>([]);
  const [text, setText] = useState<string>("");

  const addItem = () => {
    if (text) {
      setItems([...items, { id: Date.now().toString(), value: text }]);
      setText("");
    }
  };

  const deleteItem = (id: string) => {
    const filteredItems = items.filter((item) => item.id !== id);
    setItems(filteredItems)
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Item List</Text>
      <TextInput
        style={styles.input}
        placeholder="New Item"
        value={text}
        onChangeText={setText}
      />
      <Button disabled={text === ""} title="Add new item" onPress={addItem} />
      <FlatList
        data={items}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => (
          <View style={styles.item}>
            <Text style={styles.value}>{item.value}</Text>
            <Button onPress={() => deleteItem(item.id)} title="Delete" />
          </View>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: "#fff",
  },
  title: {
    fontSize: 24,
    margin: 50,
    textAlign: "center",
  },
  input: {
    borderColor: "#ccc",
    borderWidth: 1,
    padding: 10,
    marginBottom: 20,
  },
  item: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 15,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
  },
  value: {
    display: "flex",
    alignSelf: "center",
  }
});

export default App;
